import http from 'http';
import {Server,Socket}from 'socket.io';
import { DeviceService } from './sevice'; 
import { Devices } from './device.interface'; 

export class SocketServer {
  private server: http.Server;
  private io: Server;
  private devices: DeviceService;
  private randomNumberInterval: NodeJS.Timeout|undefined;

  constructor(initialData: Devices) {
    this.server = http.createServer();
    this.io =new Server(this.server,{cors:{origin:'*'}});
    this.devices = new DeviceService(initialData);

    this.setupSocketEvents();
    this.startServer();
  }

  private setupSocketEvents(): void {
    this.io.on('connection', (socket) => {
      console.log('Un cliente se ha conectado');
      console.log('El nuevo cliente es: ' + socket.id);
  
      this.io.emit('welcome', '¡Bienvenido al servidor socket de Carlos y Duvan! 😎😎😎');
  
      this.io.emit('eventosDisponibles',
        'welcome,hello,saludo,generateNumber,random-number,error-message,getColorName,color-name');
  
      socket.on('hello', (msg) => {
        console.log(msg);
        this.io.emit('saludo', `El servidor dice hola\nTu mensaje es: ${msg}`);
      });
  
      
  
      socket.on('generateNumber', (state) => {
        if (state === 'start' && !this.randomNumberInterval) {
          console.log('Comenzando la generación de números aleatorios');
          this.randomNumberInterval = setInterval(() => {
            const randomNumber = Math.floor(Math.random() * (10 + 1));
            this.io.emit('random-number', randomNumber);
          }, 5000);
        } else if (state === 'stop' && this.randomNumberInterval) {
          console.log('Deteniendo la generación de números aleatorios');
          clearInterval(this.randomNumberInterval);
          this.randomNumberInterval = undefined;
        } else {
          console.log('Error: Argumento no válido. Debe ser "start" o "stop".');
          this.io.emit('error-message', 'Argumento no válido. Debe ser "start" o "stop".');
        }
      });
  
      socket.on('getColorName', (number) => {
        const colorNames = ["Rojo", "Naranja", "Amarillo", "Verde",
          "Azul", "Índigo", "Violeta", "Negro", "Blanco", "Gris", 'Caramelo'];
        const parsedNumber = parseInt(number);
  
        if (isNaN(parsedNumber)) {
          console.log('Error: El argumento no es un número válido.');
          socket.emit('error-message', 'El argumento no es un número válido.');
          return;
        }

        if (parsedNumber < 0 || parsedNumber > 10) {
          console.log('Error: El número está fuera del rango válido (0-10).');
          socket.emit('error-message', 'El número está fuera del rango válido (0-10).');
          return;
        }

        const colorName = colorNames[parsedNumber];
        console.log(`El nombre del color para el número ${parsedNumber} es: ${colorName}`);
        socket.emit('color-name', colorName);
      });

      socket.on('getDevices', () => {
        
          this.io.emit('dispositivos',this.devices.getDevices())
        
      });
      socket.on('setDevicestate',(data)=>{
        const {user, category, device, newState }: {user:string, category: keyof Devices; device:keyof Devices[keyof Devices]; newState: string } = data;
        this.io.emit('dispositivos',this.devices.update(user,category,device,newState))
      })

      socket.on('disconnect', () => {
        console.log('El cliente se ha desconectado');
        if (this.randomNumberInterval) {
          clearInterval(this.randomNumberInterval);
          this.randomNumberInterval = undefined;
        };
      });
    });
  }


  private startServer(): void {
    const port = 3500; // Puedes ajustar el puerto según sea necesario
    this.server.listen(port, () => {
      console.log(`Servidor de Socket.io escuchando en el puerto ${port}`);
    });
  }
}
