import { BaseDevice, Devices } from "./device.interface"; 

// Clase DeviceService que implementa la interfaz
export class DeviceService {
  private devices: Devices;

  constructor(initialData: Devices) {
    this.devices = initialData;
  }

  getDevices(): Devices {
    return this.devices;
  }
  update(user:string,category: keyof Devices, device:keyof Devices[keyof Devices], newState: string): string|Devices {
    console.log(user, category, device, newState)
    if(user==='admin'){const categoryDevices = this.devices[category];
    if (categoryDevices && categoryDevices[device]) {
      (categoryDevices[device] as BaseDevice).state = newState;
      return this.devices;
    } else {
      return `Error: La categoría '${category}' o el dispositivo '${device}' no existe.`;
    }}else {
      return'you are not an administrator'
    }
  }
}
