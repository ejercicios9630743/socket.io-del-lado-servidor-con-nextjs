export interface Devices {
  multimedia: MultimediaDevices;
  appliances: AppliancesDevices;
  computing: ComputingDevices;
}

export interface BaseDevice {
  state: string;
  brand: string;
}

interface MultimediaDevices {
  tv: BaseDevice & {
    model: string;
    inches: number;
  };
  soundSystem: BaseDevice & {
    type: string;
    power: string;
  };
}

interface AppliancesDevices {
  refrigerator: BaseDevice & {
    type: string;
    capacity: string;
  };
  washingMachine: BaseDevice & {
    type: string;
    capacity: string;
  };
  airConditioner: BaseDevice & {
    temp: string;
    coolingCapacity: string;
    type:string;
  };
}

interface ComputingDevices {
  laptop: BaseDevice & {
    model: string;
    processor: string;
    ram: string;
  };
  printer: BaseDevice & {
    type: string;
    color: boolean;
  };
}
