import { Devices } from './device.interface';
import { SocketServer } from './socketServer';

// Datos iniciales
const initialData: Devices = {
  multimedia: {
    tv: {
      state: 'off',
      brand: 'Samsung',
      model: 'Smart TV',
      inches: 55
    },
    soundSystem: {
      state: 'on',
      brand: 'Sony',
      type: 'Soundbar',
      power: '120W'
    }
  },
  appliances: {
    refrigerator: {
      state: 'on',
      brand: 'LG',
      type: 'Double door',
      capacity: '500 liters'
    },
    washingMachine: {
      state: 'off',
      brand: 'Whirlpool',
      type: 'Front load',
      capacity: '8 kg'
    },
    airConditioner: {
      state: 'off',
      temp: '16',
      brand: 'Carrier',
      type: 'Split AC',
      coolingCapacity: '12000 BTU'
    }
  },
  computing: {
    laptop: {
      state: 'on',
      brand: 'Dell',
      model: 'XPS 13',
      processor: 'Intel Core i7',
      ram: '16GB'
    },
    printer: {
      state: 'off',
      brand: 'HP',
      type: 'Inkjet',
      color: true
    }
  }
};

// Crear una instancia del servidor Socket
const socketServer = new SocketServer(initialData);
